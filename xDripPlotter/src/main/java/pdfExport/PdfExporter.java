package pdfExport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.ColumnDocumentRenderer;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.AreaBreakType;
import com.itextpdf.layout.renderer.DocumentRenderer;

import dataModel.DateTimeUtility;
import dataModel.dataObjects.BgReadings;
import guiEventHandlers.NewPdfPageHandler;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;

public class PdfExporter {

	private PdfWriter writer;
	private PdfDocument pdfDocument;
	private Document document;
	private boolean isDocStart;

	public PdfExporter(String targetDestination) {
		try {
			this.writer = new PdfWriter(new File(targetDestination) + "/xPlot Auswertung.pdf");
			this.pdfDocument = new PdfDocument(writer);
			this.document = new Document(pdfDocument, PageSize.A4);
			this.isDocStart = true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setSiteLayout() {

		// Set page border and margins
		// TODO move margins and stuff like that to cfg or at least Constants file
		document.setMargins(80, 80, 80, 80);
		pdfDocument.addEventHandler(PdfDocumentEvent.START_PAGE, new NewPdfPageHandler());
	}

	/**
	 * Convert JavaFX image and write it to pdf. Note that all charts will be
	 * rotated by 90° by default; scaling is applied accordingly.
	 * 
	 * @param chartImage
	 */
	public void writeCharts(WritableImage chartImage) {
		if (!isDocStart) {
			document.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		} else {
			isDocStart = false;
		}
		DocumentRenderer renderer = new DocumentRenderer(document);
		document.setRenderer(renderer);
		document.add(new AreaBreak(AreaBreakType.LAST_PAGE));
		try {
			java.awt.Image awtImage = SwingFXUtils.fromFXImage(chartImage, null);
			Image iTextImage = new Image(ImageDataFactory.create(awtImage, null));
			iTextImage.setRotationAngle(1.5708); // Why do they use radiants instead of degrees???
			iTextImage.scaleToFit((float) (PageSize.A4.getWidth() * 1.1), (float) (PageSize.A4.getHeight() * 1.1));
			document.add(iTextImage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void writeTable(ObservableList<BgReadings> readings) {
		// Exit if readings is empty (possible/unavoidable under certain circumstances)
		if (readings.isEmpty()) {
			System.out.println("No bloodglucose readings available during given time, PDF table generation exited.");
			return;
		}
		// Setup page (column layout)
		document.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
		float colWidth = (PageSize.A4.getWidth() - 160) / 3 - 15;
		float colHeight = PageSize.A4.getHeight() - 160;
		Rectangle[] cols = new Rectangle[3];
		for (int i = 0; i < 3; i++) {
			float xCoord = 80 + i * colWidth + i * 5;
			cols[i] = new Rectangle(xCoord, 80, colWidth, colHeight);
		}
		ColumnDocumentRenderer tableRenderer = new ColumnDocumentRenderer(document, cols);
		document.setRenderer(tableRenderer);
		document.add(new AreaBreak(AreaBreakType.LAST_PAGE));

		// New table and header for each day
		PdfFont headerFont = null;
		try {
			headerFont = PdfFontFactory.createFont(StandardFonts.HELVETICA_BOLDOBLIQUE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LocalDateTime currentDay = DateTimeUtility.toLocalDateTime(readings.get(0).getTimestamp());
		Table currentTable = new Table(2);
		
		for (int i = 0; i < readings.size(); i++) {
			// "Next-day-logic": start a new section with header etc. for each day
			if (DateTimeUtility.toLocalDateTime(readings.get(i).getTimestamp()).getDayOfWeek().compareTo(currentDay.getDayOfWeek()) > 0) {
				document.add(currentTable);
				currentDay = DateTimeUtility.toLocalDateTime(readings.get(i).getTimestamp());
				Paragraph header = new Paragraph(DateTimeUtility.formatLocalDateTime(currentDay, "EE, dd. MMM yyyy"));
				header.setFont(headerFont);
				document.add(header);
				currentTable = new Table(2);
			}
			Cell bgCell = new Cell();
			Cell timeCell = new Cell();

			String bgCellContent = Long.toString(Math.round(readings.get(i).getCalculated_value()));
			bgCell.add(new Paragraph(bgCellContent));
			String timeCellContent = DateTimeUtility
					.formatLocalDateTime(DateTimeUtility.toLocalDateTime(readings.get(i).getTimestamp()), "hh:mm");
			timeCell.add(new Paragraph(timeCellContent));

			currentTable.addCell(bgCell);
			currentTable.addCell(timeCell);
		}

	}

	/**
	 * Call this method once all pdf elements have been put into the document and
	 * layout is finalized. Its one and only job is to close all (pdf) documents and
	 * clean up.
	 */
	public final void terminate() {
		document.close();
		pdfDocument.close();
	}

}

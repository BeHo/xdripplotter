package main;

import guiLauncher.ApplicationLauncher;
import javafx.application.Application;

/**
 * Starting point for xPlot. This class has just one purpose: launch the
 * application without having to extend application directly
 * 
 * @author Benedikt Hotz
 *
 */
public class Main {

	/**
	 * Main method, launching the JavaFX apllication in another class ({@link ApplicationLauncher})
	 * @param args
	 */
	public static void main(String[] args) {
		Application.launch(ApplicationLauncher.class);
	}

}

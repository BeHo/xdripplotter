package guiController;

import java.beans.PropertyChangeSupport;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import Configuration.Constants;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;

/**
 * Instances of this class are used in every view / tab. It manages some GUI
 * controls to change date and time of displayed blood glucose values, including
 * two date pickers (from/to dates) and a slider (amount of days from starting
 * date). The class synchronizes these controls, so the slider won't show a
 * different timespan than the two date pickers. Illegal dates (e.g. dates
 * before the earliest available reading in the database) will be
 * filtered/disabled for selection.
 * 
 * @author Benedikt Hotz
 *
 */
public class TimeBarController {

	private PropertyChangeSupport notifier = new PropertyChangeSupport(this);
	private LocalDate earliestReading;
	private LocalDate latestReading;

	@FXML
	private DatePicker dpckFrom;

	@FXML
	private DatePicker dpckTo;

	@FXML
	private Slider sldTimespan;

	@FXML
	private Label lblNumberDays;

	public LocalDate getStartDate() {
		return dpckFrom.getValue();
	}

	public LocalDate getEndDate() {
		return dpckTo.getValue();
	}

	public void setEarliestReading(LocalDate earliestReading) {
		this.earliestReading = earliestReading;
	}

	public void setLatestReading(LocalDate latestReading) {
		this.latestReading = latestReading;
	}

	/**
	 * When the bar is initilized, add a listener (updating the other time bar GUI
	 * elements), as well as an event handler (issuing the actual change event
	 * resulting in a new database call and refreshing the views) to the slider.
	 * Just switching the time slider around must not result in a new database call
	 * each time - that kills performance. So the update event needs to be treated
	 * differently and just when the user releases the mouse button.
	 */
	@FXML
	void initialize() {
		sldTimespan.valueProperty()
				.addListener((observableValue, oldVal, newVal) -> updateGuiWidgets(newVal.doubleValue()));
		sldTimespan.setOnMouseReleased(event -> {
			dpckTo.setValue(dpckFrom.getValue().plusDays(Math.round(sldTimespan.getValue())));
			notifier.firePropertyChange(Constants.PROPERTY_CHANGE_TIMESPAN, null, sldTimespan);
		});
	}

	/**
	 * Disable date cells in date pickers that are outside the database's time
	 * scope.
	 */
	public void setDatePickerCellFactories() {

		dpckFrom.setDayCellFactory(pickerFrom -> new DateCell() {
			@Override
			public void updateItem(LocalDate currentDate, boolean empty) {
				super.updateItem(currentDate, empty);
				if (currentDate.isBefore(earliestReading) || currentDate.isAfter(latestReading) || empty) {
					setDisable(true);
				}
			}

		});

		dpckTo.setDayCellFactory(pickerTo -> new DateCell() {
			@Override
			public void updateItem(LocalDate currentDate, boolean empty) {
				super.updateItem(currentDate, empty);
				if (currentDate.isBefore(earliestReading) || currentDate.isAfter(latestReading) || empty) {
					setDisable(true);
				}
			}

		});
	}

	/**
	 * Called by MainGuiController; adds the latter as a listener to this class. =>
	 * date changes can be handled directly by the controller
	 * 
	 * @param listener - MainGuiController
	 */
	public void setListener(MainGuiController listener) {
		notifier.addPropertyChangeListener(listener);
	}

	/**
	 * Call this to force set a specified date in DatePicker and coherent widgets
	 * from inside the program. The given date will be displayed as is inside the
	 * "to" Datepicker, whereas the "from" picker will show the given date minus the
	 * value specified in Constants.DEFAULT_TIMESPAN. This value will also be set in
	 * the timespan slider and its respective label.
	 * 
	 * @param date - {@link LocalDate}
	 */
	public void setCustomDate(LocalDate date) {
		dpckTo.setValue(date);
		LocalDate begin = date.minusDays(Constants.DEFAULT_TIMESPAN);
		dpckFrom.setValue(begin);
		lblNumberDays.setText(Constants.DEFAULT_TIMESPAN + " Tage");
		sldTimespan.setValue(Constants.DEFAULT_TIMESPAN);
	}

	/**
	 * This method should be called from timespan slider's ChangeListener class. It
	 * just displays the date represented by current "from" date plus the amount of
	 * days currently set in the slider. Actual data changes are triggered only on
	 * mouse button release (in order to save superfluous database calls and improve
	 * performance)
	 * 
	 * @param timespan - double
	 */
	public void updateGuiWidgets(double timespan) {
		lblNumberDays.setText(Math.round(timespan) + " Tage");
	}

	/**
	 * * Event handler for DatePicker (end date) changes. Makes sure the date is
	 * valid (warning is displayed otherwise), resets the timespan slider
	 * accordingly and fires a property changed event, which is used by the
	 * MainGuiController to trigger view updates. Works exactly like
	 * dpckTo_dateChanged()
	 * 
	 * @param event - ActionEvent
	 */
	@FXML
	void dpckFrom_dateChanged(ActionEvent event) {
		if (dpckFrom.getValue() == null || dpckTo.getValue() == null) {
			return; // this is the case e.g. on loading a new database
		}

		// Trim date if this date is after end date
		if (dpckFrom.getValue().isAfter(dpckTo.getValue())) {
			dpckFrom.setValue(dpckTo.getValue());
			sldTimespan.setValue(0);
			showDateInvalidWarning();
		}
		sldTimespan.setValue(ChronoUnit.DAYS.between(dpckFrom.getValue(), dpckTo.getValue()));
		notifier.firePropertyChange(Constants.PROPERTY_CHANGE_FROM_DATE, null, dpckFrom);
	}

	/**
	 * Event handler for DatePicker (start date) changes. Makes sure the date is
	 * valid (warning is displayed otherwise), resets the timespan slider
	 * accordingly and fires a property changed event, which is used by the
	 * MainGuiController to trigger view updates. Works exactly like
	 * dpckFrom_dateChanged()
	 * 
	 * @param event - ActionEvent
	 */
	@FXML
	void dpckTo_dateChanged(ActionEvent event) {
		// Event fired on empty picker (e.g. on loading a new DB)
		if (dpckFrom.getValue() == null || dpckTo.getValue() == null) {
			return;
		}

		// Verify the date
		if (dpckTo.getValue().isBefore(dpckFrom.getValue())) {
			dpckTo.setValue(dpckFrom.getValue());
			sldTimespan.setValue(0);
			showDateInvalidWarning();
		}
		// Handle the event
		sldTimespan.setValue(ChronoUnit.DAYS.between(dpckFrom.getValue(), dpckTo.getValue()));
		notifier.firePropertyChange(Constants.PROPERTY_CHANGE_TO_DATE, null, dpckTo);
	}

	/**
	 * Display a warning massage if the user chose an end date that is set before
	 * the start date or vice versa
	 */
	public void showDateInvalidWarning() {
		Alert dateInvalid = new Alert(AlertType.WARNING);
		dateInvalid.setTitle("Datumseingabe ungültig!");
		dateInvalid.setContentText(
				"Das eingegebene Datum ist ungültig: der 'von' Wert darf nicht nach dem 'bis' Wert liegen und umgekehrt! In diesem Fall wird automatisch der Wert des jeweils anderen Datumsfeldes gesetzt (also z.B. der 'bis'-Wert ins Feld 'von' als max. mögliche Zeitspanne)");
		dateInvalid.setHeaderText(null);
		dateInvalid.show();
	}

}

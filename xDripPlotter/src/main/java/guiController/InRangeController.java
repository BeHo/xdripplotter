package guiController;

import java.util.HashMap;

import Configuration.Constants;
import dataModel.InRangeCalculator;
import dataModel.dataObjects.BgReadings;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;

public class InRangeController extends ViewController {

	private PieChart chart;

	@FXML
	private Pane inRangeContainer;

	@FXML
	void initialize() {
	}

	@Override
	public void refreshView(ObservableList<BgReadings> readings) {
		ObservableList<PieChart.Data> chartData = FXCollections.observableArrayList();
		// Clean up first, view is messed up after refresh otherwise.
		inRangeContainer.getChildren().clear();

		// Calculate the readings
		InRangeCalculator calculator = new InRangeCalculator(readings);
		HashMap<Constants.IN_RANGE_VALUES, Integer> stats = calculator.calculateReadingsInRange();
		chartData.add(new PieChart.Data("Unter Ziel", stats.get(Constants.IN_RANGE_VALUES.BELOW)));
		chartData.add(new PieChart.Data("Im Zielbereich", stats.get(Constants.IN_RANGE_VALUES.IN)));
		chartData.add(new PieChart.Data("Über Ziel", stats.get(Constants.IN_RANGE_VALUES.ABOVE)));

		// Create new chart and add to parent
		this.chart = new PieChart(chartData);
		chartData.forEach(dataObj -> dataObj.nameProperty()
				.bind(Bindings.concat(Math.round(dataObj.getPieValue()), "% ", dataObj.getName())));
		chart.setLegendVisible(false);
		chart.setTitle("Werte im Zielbereich - Prozentuale Verteilung");

		inRangeContainer.getChildren().add(chart);
	}

	public WritableImage chartToImage() {
		return chart.snapshot(null, null);
	}
}

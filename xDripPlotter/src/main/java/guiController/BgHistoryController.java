package guiController;

import dataModel.BgHistoryCalculator;
import dataModel.dataObjects.BgReadings;
import guiController.extendedFxClasses.ColoredLineChart;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.NumberAxis;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;

public class BgHistoryController extends ViewController {

	private ColoredLineChart chart;

	@FXML
	private AnchorPane bgHistoryContainer;

	@FXML
	void initialize() {
	}

	@Override
	public void refreshView(ObservableList<BgReadings> readings) {
		// Clean up first, view is messed up after refresh otherwise.
		bgHistoryContainer.getChildren().clear();

		NumberAxis xAxis = new NumberAxis();
		NumberAxis yAxis = new NumberAxis();
		xAxis.setAutoRanging(false);
		xAxis.setUpperBound(1440);
		xAxis.setTickUnit(60);
		xAxis.setTickLabelFormatter(new StringConverter<Number>() {

			@Override
			public String toString(Number object) {
				int hour = object.intValue() / 60;
				int minute = object.intValue() % 60 * 60;
				return String.format("%02d", hour) + ":" + String.format("%02d", minute);
			}

			@Override
			public Number fromString(String string) {
				return 0;
			}
		});

		this.chart = new ColoredLineChart(xAxis, yAxis);
		chart.setTitle("Blutzuckerverlauf im angegebenen Zeitraum");
		BgHistoryCalculator calculator = new BgHistoryCalculator(readings);
		chart.getData().addAll(calculator.calculateSeries());

		bgHistoryContainer.getChildren().add(chart);
		chart.addClearViewListener();
		AnchorPane.setBottomAnchor(chart, 0.0);
		AnchorPane.setTopAnchor(chart, 0.0);
		AnchorPane.setLeftAnchor(chart, 0.0);
		AnchorPane.setRightAnchor(chart, 0.0);
	}

	public WritableImage chartToImage() {
		return chart.snapshot(null, null);
	}
}

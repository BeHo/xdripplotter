package guiController;

import java.time.LocalDateTime;

import Configuration.Constants;
import Configuration.propertiesHandler;
import dataModel.DateTimeUtility;
import dataModel.dataObjects.BgReadings;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

public class AllValuesController extends ViewController {

	private ObservableList<BgReadings> currentReadings;

	@FXML
	private AnchorPane allValuesContainer;

	public ObservableList<BgReadings> getCurrentReadings() {
		return currentReadings;
	}

	@FXML
	void initialize() {
	}

	@Override
	public void refreshView(ObservableList<BgReadings> newReadings) {
		this.currentReadings = newReadings;
		TableView<BgReadings> tvAllValues = new TableView<BgReadings>();
		TableColumn<BgReadings, Double> tcolValue = new TableColumn<BgReadings, Double>("Blutzucker");
		TableColumn<BgReadings, Long> tcolTime = new TableColumn<BgReadings, Long>("Datum/Uhrzeit");

		tcolValue.setCellValueFactory(readings -> {
			return new SimpleObjectProperty<Double>(readings.getValue().getCalculated_value());
		});
		tcolValue.setCellFactory(column -> {
			return new TableCell<BgReadings, Double>() {
				@Override
				protected void updateItem(Double item, boolean empty) {
					super.updateItem(item, empty);
					if (item == null || empty) {
						setText(null);
						setStyle("");
					} else {
						// Reset table style - very important: otherwise, cell rendering is completely
						// messed up! (Colored cells apparently overlap with non-colored ones, so
						// everything is red at some point
						setStyle("");

						int value = (int) Math.round(item);
						setText(String.valueOf(value));
						if (value > Integer
								.parseInt(propertiesHandler.getInstance().getPropteryByKey("Threshold_high"))) {
							setStyle("-fx-background-color: #f54242; -fx-text-fill: white;");
						}
						if (value < Integer.parseInt(propertiesHandler.getInstance().getPropteryByKey("Threshold_low"))) {
							setStyle("-fx-background-color: #6fd1f7");
						}
					}
				}
			};
		});
		tcolTime.setCellValueFactory(readings -> {
			return new SimpleObjectProperty<Long>(readings.getValue().getTimestamp());
		});
		tcolTime.setCellFactory(column -> {
			return new TableCell<BgReadings, Long>() {
				@Override
				protected void updateItem(Long item, boolean empty) {
					super.updateItem(item, empty);
					if (item == null || empty) {
						setText(null);
						setStyle("");
					} else {
						LocalDateTime time = DateTimeUtility.toLocalDateTime(item);
						setText(DateTimeUtility.formatLocalDateTime(time, Constants.DATETIMEFORMAT));
					}
				}
			};
		});
		tvAllValues.setItems(newReadings);
		tvAllValues.getColumns().add(tcolTime);
		tvAllValues.getColumns().add(tcolValue);
		allValuesContainer.getChildren().add(tvAllValues);
		AnchorPane.setBottomAnchor(tvAllValues, 0.0);
		AnchorPane.setTopAnchor(tvAllValues, 0.0);
		AnchorPane.setLeftAnchor(tvAllValues, 0.0);
		AnchorPane.setRightAnchor(tvAllValues, 0.0);
	}

}
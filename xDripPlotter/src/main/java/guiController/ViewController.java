package guiController;

import dataModel.dataObjects.BgReadings;
import javafx.collections.ObservableList;

public abstract class ViewController {

	public abstract void refreshView(ObservableList<BgReadings> readings);
}

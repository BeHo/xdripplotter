package guiController;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.jfoenix.controls.JFXButton;

import Configuration.Constants;
import Configuration.propertiesHandler;
import dataModel.DatabaseConnector;
import dataModel.DateTimeUtility;
import dataModel.dataObjects.BgReadings;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pdfExport.PdfExporter;

/**
 * This class works as a central administration point for all underlying GUI
 * widgets (which may be outsourced to separate FXML files, for instance), as
 * well as a single point of access to the database. Hence, if the user performs
 * any action on any GUI widget, the changed data will be reported by the
 * responsible controller class to this class. Then, if necessary, database
 * changes/readings are issued by this class, as well as data calculations on
 * the data layer. Finally, the updated or modified data are passed on to
 * controller classes managing widgets which are merely used for displaying data
 * (thus not receiving user action)
 * 
 * @author Benedikt Hotz
 *
 */
public class MainGuiController implements PropertyChangeListener {

	private DatabaseConnector connector;
	private HashMap<TimeBarController, ViewController> timeBarToContainerMapping = new HashMap<TimeBarController, ViewController>();
	private LocalDate earliestReading;
	private LocalDate latestReading;

	@FXML
	private JFXButton btnOpenDb;

	@FXML
	private JFXButton btnPdfExport;

	@FXML
	private JFXButton btnSettings;

	@FXML
	private Tab tbBgHistory;

	@FXML
	private Tab tbInRange;

	@FXML
	private Tab tbAllValues;

	@FXML
	private Tab tbExtremeValues;

	@FXML
	private Parent allValuesContainer;

	@FXML
	private Parent bgHistoryContainer;

	@FXML
	private BgHistoryController bgHistoryContainerController;

	@FXML
	private Parent inRangeContainer;

	@FXML
	private InRangeController inRangeContainerController;

	@FXML
	private AllValuesController allValuesContainerController;

	@FXML
	private TimeBarController timeBar_bgHistoryController;

	@FXML
	private TimeBarController timeBar_inRangeController;

	@FXML
	private TimeBarController timeBar_allValuesController;

	@FXML
	private VBox vbAllValuesWrapper;

	@FXML
	private Parent extValueMonitorContainer;

	@FXML
	private ExtValueMonitorContainerController extValueMonitorContainerController;

	@FXML
	private TimeBarController timeBar_extValuesController;

	/**
	 * Event handler method for button "Datenbank öffnen". Shows a file picker
	 * returning the database file, which may then be used to grab data from DB and
	 * initialize several views.
	 * 
	 * @param event - ActionEvent
	 */
	@FXML
	void btnClick_btnOpenDb(ActionEvent event) {
		FileChooser chooser = new FileChooser();
		File dbFile = chooser.showOpenDialog(btnOpenDb.getScene().getWindow());

		// If selected file exists - wrong file type however still results in exception
		if (dbFile != null) {
			getInitialReadings(dbFile);
		}
	}

	/**
	 * Event handler for Button "Einstellungen". Opens the settings dialogue
	 * allowing the user to change basic settings. Forces all views to be refreshed,
	 * since changed user preferences may result in graphic changes.
	 */
	@FXML
	void btnClick_btnSettings(ActionEvent event) {
		try {
			Stage stage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource(Constants.USERPREF_FXML_LOCATION));
			Parent root = loader.load();
			stage.setTitle("Einstellungen");
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Helper method to bundle Db calls for initial readings, calling next helper
	 * method
	 * 
	 * @param dbFile - {@link File}
	 */
	private void getInitialReadings(File dbFile) {
		this.connector = new DatabaseConnector(dbFile.getAbsolutePath());

		// Get the latest date in the database. Starting point for all operations.
		this.latestReading = connector.getMaxMinDataset(Constants.DATABASE_MAX_MIN.LATEST);
		this.earliestReading = connector.getMaxMinDataset(Constants.DATABASE_MAX_MIN.EARLIEST);
		initializeTimeBar(latestReading);

		ObservableList<BgReadings> initialReadings = connector.getReadings(
				latestReading.minusDays(Constants.DEFAULT_TIMESPAN).atStartOfDay(), latestReading.atStartOfDay());

		// Initialize the views, i.e. set charts and tables with initial values.
		initViews(initialReadings);
	}

	/**
	 * Helper method; every view consists of a time bar and a chart/table/whatever
	 * container. Both elements are defined in separate FXML files and have
	 * different controller classes accordingly. These time bars have to be
	 * initialized at program start with values from the database, which is done
	 * here.
	 * 
	 * @param latestReading
	 */
	private void initializeTimeBar(LocalDate latestReading) {

		// Set the starting dates for all tabs
		timeBar_allValuesController.setCustomDate(latestReading);
		timeBar_bgHistoryController.setCustomDate(latestReading);
		timeBar_inRangeController.setCustomDate(latestReading);

		// Pass earliest and latest reading to TimeBar (needed for DatePickers to
		// determine the allowed range of selectable dates)
		timeBar_allValuesController.setEarliestReading(earliestReading);
		timeBar_allValuesController.setLatestReading(latestReading);
		timeBar_allValuesController.setDatePickerCellFactories();
		timeBar_bgHistoryController.setEarliestReading(earliestReading);
		timeBar_bgHistoryController.setLatestReading(latestReading);
		timeBar_bgHistoryController.setDatePickerCellFactories();
		timeBar_inRangeController.setEarliestReading(earliestReading);
		timeBar_inRangeController.setLatestReading(latestReading);
		timeBar_inRangeController.setDatePickerCellFactories();

	}

	/**
	 * Helper method; draw charts etc. with initial values.
	 * 
	 * @param initialReadings - ObservableList[BgReadings]
	 */
	private void initViews(ObservableList<BgReadings> initialReadings) {
		allValuesContainerController.refreshView(initialReadings);
		inRangeContainerController.refreshView(initialReadings);
		bgHistoryContainerController.refreshView(initialReadings);
		extValueMonitorContainerController.refreshView(initialReadings);
	}

	/**
	 * Event handler method for button "PDF Export". Opens a directory picker, then
	 * issues layouting and writing calls to the PDF creator class.
	 * 
	 * @param event - ActionEvent
	 */
	@FXML
	void btnClick_btnPdfExport(ActionEvent event) {
		DirectoryChooser chooser = new DirectoryChooser();
		File file = chooser.showDialog(btnPdfExport.getScene().getWindow());
		PdfExporter exporter = new PdfExporter(file.getAbsolutePath());

		// Create the pdf
		exporter.setSiteLayout();
		exporter.writeCharts(bgHistoryContainerController.chartToImage());
		exporter.writeCharts(inRangeContainerController.chartToImage());
		exporter.writeTable(allValuesContainerController.getCurrentReadings());
		exporter.terminate();
	}

	/**
	 * Do the preliminary work at program start: set some icons, link this class
	 * with time bar controllers to enable exchange of data, make sure the user has
	 * set necessary preferences.
	 */
	@FXML
	void initialize() {
		checkUserPresets();

		setPropertyChangeListener();

		// Connect time bar controller and view container, so calls can be passed
		// forward correctly
		mapTimeBarControllerToContainer();

		// Insert icons
		Image inRangeIcon = new Image("/pic/piechart.png");
		Image bgHistoryIcon = new Image("/pic/linechart.png");
		Image allValuesIcon = new Image("/pic/table.png");
		Image extremeValuesIcon = new Image("/pic/extreme.png");
		tbInRange.setGraphic(new ImageView(inRangeIcon));
		tbBgHistory.setGraphic(new ImageView(bgHistoryIcon));
		tbAllValues.setGraphic(new ImageView(allValuesIcon));
		tbExtremeValues.setGraphic(new ImageView(extremeValuesIcon));
	}

	/**
	 * Check, if a properties file exists and if the necessary preferences have been
	 * set. If not, prompt the user for input.
	 */
	private void checkUserPresets() {
		File props = new File(Constants.CONFIG_FILE_LOC);
		if (!props.exists()) {
			showNoPropertiesInfo();
			openPreferencesDialog();
		} else {
			ArrayList<String> missingProperties = new ArrayList<String>();
			for (Entry<String, String> entry : propertiesHandler.getInstance().getPropertyList().entrySet()) {
				if (entry.getValue() == null || "".equals(entry.getValue())) {
					missingProperties.add(entry.getKey());
				}
			}
			if (!missingProperties.isEmpty()) {
				showPropertyMissingWarning(missingProperties);
				openPreferencesDialog();
			}
		}
	}

	private void showNoPropertiesInfo() {
		Alert noProps = new Alert(AlertType.INFORMATION);
		noProps.setTitle("Benutzerdefinierte Einstellungen setzen");
		noProps.setHeaderText(null);
		noProps.setContentText(
				"Es wurden keine benutzerdefinierten Therapieeinstellungen gefunden. Bitte nehmen Sie im folgenden Dialog Eingaben entsprechend Ihren Therapieeinstellungen vor.");
		noProps.showAndWait();
	}

	private void showPropertyMissingWarning(ArrayList<String> missingProperties) {
		Alert propMissing = new Alert(AlertType.WARNING);
		propMissing.setTitle("Einstellung fehlt");
		propMissing.setHeaderText(null);
		StringBuilder builder = new StringBuilder("Folgende Benutzereinstellungen fehlen:\n");
		for (String property : missingProperties) {
			builder.append(property);
			builder.append("\n");
		}
		builder.append(
				"Bitte tragen Sie die fehlenden Werte im folgenden Dialog ein. Bestimmte Auswertungsfunktionen funktionieren sonst nicht korrekt!");
		propMissing.setContentText(builder.toString());
		propMissing.showAndWait();
	}

	private void openPreferencesDialog() {
		try {
			Stage stage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource(Constants.USERPREF_FXML_LOCATION));
			Parent root = loader.load();
			stage.setTitle("Benutzerdefinierte Einstellungen");
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setAlwaysOnTop(true);
			stage.show();
		} catch (IOException e) {
			System.err.println("Could not find fxml file at given location: " + Constants.CONFIG_FILE_LOC);
		}
	}

	/**
	 * Add this class as listener to all time bar controllers. Replaces Oberserver
	 * since it's deprecated. Reason for connection => refer to doc of
	 * propertyChange()
	 */
	private void setPropertyChangeListener() {
		timeBar_allValuesController.setListener(this);
		timeBar_bgHistoryController.setListener(this);
		timeBar_inRangeController.setListener(this);
	}

	/**
	 * Map every TimeBar to its respective container. This is necessary so this
	 * class knows which container to pass updated TimeBar values. First value is
	 * TimeBar, second is container.
	 */
	private void mapTimeBarControllerToContainer() {
		timeBarToContainerMapping.put(timeBar_allValuesController, allValuesContainerController);
		timeBarToContainerMapping.put(timeBar_bgHistoryController, bgHistoryContainerController);
		timeBarToContainerMapping.put(timeBar_inRangeController, inRangeContainerController);
	}

	/**
	 * Change Listener method called by time bar controllers once the date was
	 * changed. Reason for this connection is that new blood glucose readings have
	 * to be read from the DB according to the new date(s). This method first gets
	 * the caller, then the call's source inside the caller (i.e. which GUI item is
	 * responsible for the change). The new date is then evaluated (if it's past or
	 * before the last/first date in the DB, which would be invalid) and clipped if
	 * necessary - a warning is shown in that case. If the date is valid, a DB call
	 * is issued and the respective view is refreshed using the new values
	 */
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		TimeBarController timeBar = (TimeBarController) evt.getSource();
		LocalDateTime from = null;
		LocalDateTime to = null;

		switch (evt.getPropertyName()) {
		case Constants.PROPERTY_CHANGE_TIMESPAN:
			Slider newVal = (Slider) evt.getNewValue();
			from = timeBar.getStartDate().atStartOfDay();
			to = timeBar.getStartDate().plusDays(Math.round(newVal.getValue())).atTime(23, 59);
			if (!isDateValid(to)) {
				to = this.latestReading.atTime(23, 59);
				showDateTruncationWarning(from, to);
			}
			break;

		case Constants.PROPERTY_CHANGE_FROM_DATE:
			DatePicker fromPicker = (DatePicker) evt.getNewValue();
			from = timeBar.getStartDate().atStartOfDay();
			to = timeBar.getEndDate().atTime(23, 59);
			if (!isDateValid(from)) {
				// This code seems unnecessary at first glance, because invalid date cells are
				// disabled by default (see DayCellFactories in TimeBarController). However,
				// users could still type invalid dates manually and press enter - this case is
				// addressed here.
				from = this.earliestReading.atStartOfDay();
				fromPicker.setValue(earliestReading);
				showDateTruncationWarning(from, to);
			}
			break;

		case Constants.PROPERTY_CHANGE_TO_DATE:
			DatePicker toPicker = (DatePicker) evt.getNewValue();
			from = timeBar.getStartDate().atStartOfDay();
			to = timeBar.getEndDate().atTime(23, 59);
			if (!isDateValid(to)) {
				to = this.latestReading.atTime(23, 59);
				toPicker.setValue(latestReading);
				showDateTruncationWarning(from, to);
			}
			break;
		}
		timeBarToContainerMapping.get(timeBar).refreshView(connector.getReadings(from, to));
	}

	/**
	 * Helper method, checks if the given date is before the date of the earliest
	 * reading or after the date of the latest reading in the database.
	 * 
	 * @param date - LocalDateTime
	 * @return true if the date given date is inside the date limits
	 */
	private boolean isDateValid(LocalDateTime date) {
		if (date.isBefore(this.earliestReading.atStartOfDay()) || date.isAfter(this.latestReading.atTime(23, 59))) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Show a popup warning for invalid dates, informs user that date has been
	 * clipped to the date of earliest or latest reading in the database.
	 * 
	 * @param from - LocalDateTime
	 * @param to   - LocalDateTime
	 */
	private void showDateTruncationWarning(LocalDateTime from, LocalDateTime to) {
		Alert truncationWarning = new Alert(AlertType.WARNING);
		truncationWarning.setTitle("Gültiger Datumsbereich überschritten");
		truncationWarning.setHeaderText(null);
		truncationWarning.setContentText(
				"Achtung, für den gewählten Zeitraum liegen teilweise keine Messergebnisse in der Datenbank vor. Der Ergebniszeitraum wurde entsprechend eingegrenzt:\nFrüheste Messung: "
						+ DateTimeUtility.formatLocalDateTime(from, Constants.DATETIMEFORMAT) + "\nSpäteste Messung: "
						+ DateTimeUtility.formatLocalDateTime(to, Constants.DATETIMEFORMAT));
		truncationWarning.show();
	}

}
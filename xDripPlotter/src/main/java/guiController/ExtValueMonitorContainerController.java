package guiController;

import dataModel.ExtremeValueCalculator;
import dataModel.dataObjects.BgReadings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

public class ExtValueMonitorContainerController extends ViewController {

	ObservableList<BgReadings> readings;

	@FXML
	private AnchorPane extValueMonitorContainer;

	@FXML
	private RadioButton rbLow;

	@FXML
	private ToggleGroup highLowSwitch;

	@FXML
	private RadioButton rbHigh;

	@FXML
	private SplitPane splpViewContainer;

	@FXML
	private VBox vbDataView;

	@FXML
	private AnchorPane apChartView;

	@FXML
	public void initialize() {
		rbLow.setUserData("low");
		rbHigh.setUserData("high");
		highLowSwitch.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

			@Override
			public void changed(ObservableValue<? extends Toggle> observable, Toggle oldState, Toggle newState) {
				doRefresh();
			}
		});
	}

	/**
	 * Method to be used by external callers, in cases where new readings are
	 * available and need to be passed to this class.
	 */
	@Override
	public void refreshView(ObservableList<BgReadings> readings) {
		this.readings = readings;
		doRefresh();
		ExtremeValueCalculator calculator = new ExtremeValueCalculator(readings);
		calculator.detectHypos();
	}

	/**
	 * Perform the refresh, i.e. redraw chart and update data.
	 */
	private void doRefresh() {
		if ("low".equals(highLowSwitch.getSelectedToggle().getUserData().toString())) {
			updateLowDataView();
			updateLowChartView();
		} else if ("high".equals(highLowSwitch.getSelectedToggle().getUserData().toString())) {
			updateHighDataView();
			updateHighChartView();
		}
	}

	private void updateHighChartView() {

	}

	private void updateHighDataView() {

	}

	private void updateLowChartView() {

	}

	private void updateLowDataView() {

	}

}

package guiController;

import java.util.HashMap;
import java.util.TreeMap;

import com.jfoenix.controls.JFXButton;

import Configuration.propertiesHandler;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class UserPrefController {

	@FXML
	private TextField tfThresholdHigh;

	@FXML
	private TextField tfThresholdLow;

	@FXML
	private TextField tfAmountExtremeValue;

	@FXML
	private TextField tfAmountVeryLow;

	@FXML
	private TextField tfThresholdVeryLow;

	@FXML
	private TextField tfChangePerFiveMins;

	@FXML
	private JFXButton btnSaveSettings;

	@FXML
	void initialize() {
		if (propertiesHandler.getInstance().getPropertyList().isEmpty()) {
			tfThresholdHigh.setText("180");
			tfThresholdLow.setText("80");
			tfAmountExtremeValue.setText("4");
			tfAmountVeryLow.setText("2");
			tfThresholdVeryLow.setText("50");
			tfChangePerFiveMins.setText("9");
		} else {
			HashMap<String, String> props = propertiesHandler.getInstance().getPropertyList();
			tfThresholdHigh.setText(props.get("Threshold_high"));
			tfThresholdLow.setText(props.get("Threshold_low"));
			tfAmountExtremeValue.setText(props.get("Amount_for_extreme_value"));
			tfAmountVeryLow.setText(props.get("Amount_for_very_low_value"));
			tfThresholdVeryLow.setText(props.get("Threshold_for_very_low_value"));
			tfChangePerFiveMins.setText(props.get("Change_per_five_Minutes"));
		}
	}

	/**
	 * Event handler for "Einstellungen speichern" button. Reads text input fields
	 * and writes its values to the properties file. Success or fail are reported in
	 * respective info popups.
	 * 
	 * @param event
	 */
	@FXML
	void btnClick_btnSaveSettings(ActionEvent event) {
		TreeMap<String, String> properties = new TreeMap<String, String>();
		properties.put("Threshold_high", tfThresholdHigh.getText());
		properties.put("Threshold_low", tfThresholdLow.getText());
		properties.put("Amount_for_extreme_value", tfAmountExtremeValue.getText());
		properties.put("Amount_for_very_low_value", tfAmountVeryLow.getText());
		properties.put("Change_per_five_Minutes", tfChangePerFiveMins.getText());
		properties.put("Threshold_for_very_low_value", tfThresholdVeryLow.getText());
		int writeSuccess = propertiesHandler.getInstance().writePropertyList(properties,
				"User's blood glucose settings");
		if (writeSuccess == 1) {
			Alert success = new Alert(AlertType.INFORMATION);
			success.setTitle("Einstellungen gespeichert");
			success.setContentText("Einstellungen wurden erfolgreich gespeichert!");
			success.setHeaderText(null);
			success.show();
		} else {
			Alert success = new Alert(AlertType.ERROR);
			success.setTitle("Fehler beim Speichern");
			success.setContentText(
					"Einstellungen konnten nicht gespeichert werden. Bitte wiederholen Sie den Vorgang (haben Sie Schreibrechte im Zielordner?)");
			success.setHeaderText(null);
			success.show();
		}
		Stage stage = (Stage) btnSaveSettings.getScene().getWindow();
		stage.close();
	}

}

package guiController.extendedFxClasses;

import java.util.ArrayList;

import Configuration.propertiesHandler;
import javafx.beans.value.ChangeListener;
import javafx.scene.Node;
import javafx.scene.chart.Axis;
import javafx.scene.chart.LineChart;
import javafx.scene.shape.Rectangle;

public class ColoredLineChart extends LineChart<Number, Number> {

	private Axis<Number> yAxis;

	public ColoredLineChart(Axis<Number> xAxis, Axis<Number> yAxis) {
		super(xAxis, yAxis);
		this.yAxis = yAxis;
	}

	@Override
	public void layoutPlotChildren() {
		super.layoutPlotChildren();

		removePoints();
		setHighMarker();
		setLowMarker();
	}

	private void removePoints() {
		for (Node n : getPlotChildren()) {
			if (n instanceof Tooltip) {
//				n.getStyleClass().clear();
				n.getStyleClass().add("Tooltip_invisible");
			}
		}
	}

	private void setHighMarker() {
		Rectangle highMarker = new Rectangle();
		highMarker.setX(0d);
		highMarker.setY(0d);
		highMarker.setWidth(getBoundsInLocal().getWidth());
		highMarker.setHeight(yAxis.getDisplayPosition(
				Integer.parseInt(propertiesHandler.getInstance().getPropteryByKey("Threshold_high"))));
		highMarker.getStyleClass().add("Bg_high");
		getPlotChildren().add(highMarker);
		highMarker.toBack();
	}

	private void setLowMarker() {
		Rectangle lowMarker = new Rectangle();
		lowMarker.setX(0d);
		lowMarker.setY(yAxis.getDisplayPosition(Integer.parseInt(propertiesHandler.getInstance().getPropteryByKey("Threshold_low"))));
		lowMarker.setWidth(getBoundsInLocal().getWidth());
		lowMarker.setHeight(getBoundsInLocal().getHeight());
		lowMarker.getStyleClass().add("Bg_low");
		getPlotChildren().add(lowMarker);
		lowMarker.toBack();
	}

	public void addClearViewListener() {

		ChangeListener<Number> listener = (observable, oldVal, newVal) -> {
			// Remove previously added markers - e.g. after window has been resized (a new
			// marker would be added in that case without removing the old one, resulting in
			// overlapping nodes). Probably, this has horrible consequences for performance,
			// but I see no other way to get rid of that issue
			ArrayList<Node> rectanglesToRemove = new ArrayList<Node>();
			for (Node child : getPlotChildren()) {
				if (child instanceof Rectangle) {
					rectanglesToRemove.add(child);
				}
			}
			getPlotChildren().removeAll(rectanglesToRemove);
		};
		yAxis.getScene().getWindow().widthProperty().addListener(listener);
		yAxis.getScene().getWindow().heightProperty().addListener(listener);
	}
}
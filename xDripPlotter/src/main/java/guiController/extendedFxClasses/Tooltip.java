package guiController.extendedFxClasses;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class Tooltip extends Pane {

	private Data<Number, Number> data;

	public Tooltip(Data<Number, Number> data) {
		this.data = data;
		setEventHandlers();
	}

	public Tooltip(Data<Number, Number> data, Node... children) {
		super(children);
		this.data = data;
//		getStyleClass().add("Tooltip_invisible");
		setEventHandlers();
	}

	private void setEventHandlers() {
		this.setOnMouseEntered(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				getStyleClass().clear();
				getStyleClass().add("Tooltip_visible");
				Label text = new Label(data.getYValue().toString());
				text.relocate(20, -40);
				text.getStyleClass().add("Tooltip_text");
				getChildren().setAll(text);
				toFront();
			}

		});
		
		this.setOnMouseExited(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				getChildren().clear();
				getStyleClass().clear();
				getStyleClass().add("Tooltip_invisible");
			}
		});
	}

}

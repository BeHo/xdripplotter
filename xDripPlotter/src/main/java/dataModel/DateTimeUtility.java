package dataModel;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * Utility for date and time operations, mainly conversions between unix
 * timestamps and human readable formats, as well as date / time formatting
 * stuff.
 * 
 * @author Benedikt Hotz
 *
 */
public interface DateTimeUtility {
	/**
	 * Returns an integer representation of the given date and time, based on the
	 * respective unix timestamp.
	 * 
	 * @param localDateTimeValue - Java 8's {@link LocalDateTime} is used
	 * @return unix timestamp as long
	 */
	static long toUnixTimestamp(LocalDateTime localDateTimeValue) {
		return localDateTimeValue.atZone(ZoneId.systemDefault()).toEpochSecond();
	}

	/**
	 * Returns a {@link LocalDateTime} object for the given unix timestamp. The
	 * timezone in use is UTC +1. <b>The timestamp has to be in milliseconds - don't
	 * forget to multiply by 1000 if necessary!</b>
	 * 
	 * @param unixTimestamp as long, <b>in milliseconds!</b>
	 * @return Date and time as {@link LocalDateTime}
	 */
	static LocalDateTime toLocalDateTime(long unixTimestamp) {
		return LocalDateTime.ofInstant(Instant.ofEpochMilli(unixTimestamp), ZoneOffset.ofHours(1));
	}

	/**
	 * Format the given {@link LocalDateTime} object using the given pattern; if the
	 * pattern is incorrect the underlying methods will throw an
	 * {@link IllegalArgumentException}
	 * 
	 * @param time    - LocalDateTime, the time and date to format
	 * @param pattern - The format to use as String
	 * @return Formatted date and time as String
	 */
	static String formatLocalDateTime(LocalDateTime time, String pattern) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		return formatter.format(time);
	}

	/**
	 * Returns a {@link LocalDate} object for the given unix timestamp. The timezone
	 * in use is UTC +1. <b>The timestamp has to be in milliseconds - don't forget
	 * to multiply by 1000 if necessary!</b>
	 * 
	 * @param unixTimestamp as long, <b>in milliseconds!</b>
	 * @return Date as {@link LocalDate}
	 */
	static LocalDate toLocalDate(long unixTimestamp) {
		return LocalDate.ofInstant(Instant.ofEpochMilli(unixTimestamp), ZoneOffset.ofHours(1));
	}

}

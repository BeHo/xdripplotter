package dataModel.dataObjects;

/**
 * Bean class representing the table BgReadings of the blood glucose database.
 * The name is is irritating because the class represents a single reading
 * rather than a bunch of readings, but again naming must follow the conventions
 * of DB mapping. Readings are stored with intervals of five minutes, unless the
 * glucose sensor fails for some reason or the bluetooth connection between
 * sensor and mobile phone is lost. Most fields are not used but have to be in
 * place for automated database mapping to work properly. Fields that are
 * actually used are timestamp and calculated_value - more might follow.
 * 
 * @author Benedikt Hotz
 *
 */
public class BgReadings {
	private int _id;
	private double a;
	private double age_adjusted_raw_value;
	private double b;
	private double c;
	private double calculated_value;
	private double calculated_value_slope;
	private int calibration;
	private int calibration_flag;
	private String calibration_uuid;
	private String dg_delta_name;
	private double dg_mgdl;
	private double dg_slope;
	private double filtered_calculated_value;
	private double filtered_data;
	private int hide_slope;
	private int snyced;
	private String noise;
	private double ra;
	private double raw_calculated;
	private double raw_data;
	private double rb;
	private double rc;
	private int sensor;
	private String sensor_uuid;
	private String source_info;
	private double time_since_sensor_started;
	private long timestamp;
	private String uuid;

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getAge_adjusted_raw_value() {
		return age_adjusted_raw_value;
	}

	public void setAge_adjusted_raw_value(double age_adjusted_raw_value) {
		this.age_adjusted_raw_value = age_adjusted_raw_value;
	}

	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}

	public double getC() {
		return c;
	}

	public void setC(double c) {
		this.c = c;
	}

	/**
	 * Returns the blood glucose value of this reading. This is not original value
	 * provided by the sensor, because some background calculations are applied to
	 * increase precision. However, that's the value to be used.
	 * 
	 * @return value as double
	 */
	public double getCalculated_value() {
		return calculated_value;
	}

	public void setCalculated_value(double calculated_value) {
		this.calculated_value = calculated_value;
	}

	public double getCalculated_value_slope() {
		return calculated_value_slope;
	}

	public void setCalculated_value_slope(double calculated_value_slope) {
		this.calculated_value_slope = calculated_value_slope;
	}

	public int getCalibration() {
		return calibration;
	}

	public void setCalibration(int calibration) {
		this.calibration = calibration;
	}

	public int getCalibration_flag() {
		return calibration_flag;
	}

	public void setCalibration_flag(int calibration_flag) {
		this.calibration_flag = calibration_flag;
	}

	public String getCalibration_uuid() {
		return calibration_uuid;
	}

	public void setCalibration_uuid(String calibration_uuid) {
		this.calibration_uuid = calibration_uuid;
	}

	public String getDg_delta_name() {
		return dg_delta_name;
	}

	public void setDg_delta_name(String dg_delta_name) {
		this.dg_delta_name = dg_delta_name;
	}

	public double getDg_mgdl() {
		return dg_mgdl;
	}

	public void setDg_mgdl(double dg_mgdl) {
		this.dg_mgdl = dg_mgdl;
	}

	public double getDg_slope() {
		return dg_slope;
	}

	public void setDg_slope(double dg_slope) {
		this.dg_slope = dg_slope;
	}

	public double getFiltered_calculated_value() {
		return filtered_calculated_value;
	}

	public void setFiltered_calculated_value(double filtered_calculated_value) {
		this.filtered_calculated_value = filtered_calculated_value;
	}

	public double getFiltered_data() {
		return filtered_data;
	}

	public void setFiltered_data(double filtered_data) {
		this.filtered_data = filtered_data;
	}

	public int getHide_slope() {
		return hide_slope;
	}

	public void setHide_slope(int hide_slope) {
		this.hide_slope = hide_slope;
	}

	public int getSnyced() {
		return snyced;
	}

	public void setSnyced(int snyced) {
		this.snyced = snyced;
	}

	public String getNoise() {
		return noise;
	}

	public void setNoise(String noise) {
		this.noise = noise;
	}

	public double getRa() {
		return ra;
	}

	public void setRa(double ra) {
		this.ra = ra;
	}

	public double getRaw_calculated() {
		return raw_calculated;
	}

	public void setRaw_calculated(double raw_calculated) {
		this.raw_calculated = raw_calculated;
	}

	public double getRaw_data() {
		return raw_data;
	}

	public void setRaw_data(double raw_data) {
		this.raw_data = raw_data;
	}

	public double getRb() {
		return rb;
	}

	public void setRb(double rb) {
		this.rb = rb;
	}

	public double getRc() {
		return rc;
	}

	public void setRc(double rc) {
		this.rc = rc;
	}

	public int getSensor() {
		return sensor;
	}

	public void setSensor(int sensor) {
		this.sensor = sensor;
	}

	public String getSensor_uuid() {
		return sensor_uuid;
	}

	public void setSensor_uuid(String sensor_uuid) {
		this.sensor_uuid = sensor_uuid;
	}

	public String getSource_info() {
		return source_info;
	}

	public void setSource_info(String source_info) {
		this.source_info = source_info;
	}

	public double getTime_since_sensor_started() {
		return time_since_sensor_started;
	}

	public void setTime_since_sensor_started(double time_since_sensor_started) {
		this.time_since_sensor_started = time_since_sensor_started;
	}

	/**
	 * Returns the time a blood glucose measurement was taken, in Unix time.
	 * 
	 * @return timestamp as long
	 */
	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}

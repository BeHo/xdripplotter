package dataModel.dataObjects;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

import Configuration.propertiesHandler;
import dataModel.DateTimeUtility;
import dataModel.ExtremeValueCalculator;

/**
 * Class representing a hypoglycemia. Whether or not we have a hypo is not
 * determined here; the class has to be provided with a list of readings that
 * match the conditions for a hypo, <b>including blood glucose values ca. 1,5
 * hours before and after the hypo itself.</b> Consequently, the start and end
 * index of the actual hypo have to be passed as well. This class then analyzes
 * these values - the outcome can be used to display various graphical
 * statistics.
 * <p>
 * <h1>Invoked by:</h1>
 * <ul>
 * <li>{@link ExtremeValueCalculator}.detectHypos()</li>
 * </ul>
 * </p>
 * 
 * @author Benedikt Hotz
 *
 */
public class Hypoglycemia {

	private ArrayList<BgReadings> readings;
	private int startIndex;
	private int endIndex;
	private LocalDateTime begin;
	private LocalDateTime end;
	private Duration duration;
	private int lowestValue;
	private boolean isSevere;
	private boolean isOvercorrectionResult;
	private boolean isOvercorrected;

	public Hypoglycemia(ArrayList<BgReadings> readings, int startIndex, int endIndex) {
		this.readings = readings;
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.isSevere = false;
//		this.begin = DateTimeUtility.toLocalDateTime(readings.get(startIndex).getTimestamp());
//		this.end = DateTimeUtility.toLocalDateTime(readings.get(endIndex).getTimestamp());
//		setDuration();
		setLowestValue();
		setSevere();
		setOvercorrectionResult();
	}

	public LocalDateTime getBegin() {
		return begin;
	}

	public LocalDateTime getEnd() {
		return end;
	}

	public Duration getDuration() {
		return duration;
	}

	public int getLowestValue() {
		return lowestValue;
	}

	public boolean isSevere() {
		return isSevere;
	}

	private void setDuration() {
		this.duration = Duration.between(begin, end);
	}

	private void setLowestValue() {
		Double lowest = readings.get(startIndex).getCalculated_value();
		for (int i = startIndex + 1; i <= endIndex; i++) {
			if (readings.get(i).getCalculated_value() < lowest) {
				lowest = readings.get(i).getCalculated_value();
			}
		}
		this.lowestValue = lowest.intValue();
	}

	/**
	 * Check if the hypo is to be considered severe, which is the case if a certain
	 * amount of values is below the defined threshold (like 50 mg/dl).
	 */
	private void setSevere() {
		int severeCounter = 0;
		for (int i = startIndex; i <= endIndex; i++) {
			if (readings.get(i).getCalculated_value() < Integer
					.parseInt(propertiesHandler.getInstance().getPropteryByKey("Threshold_for_very_low_value"))) {
				severeCounter++;
			}
			if (severeCounter >= Integer
					.parseInt(propertiesHandler.getInstance().getPropteryByKey("Threshold_for_very_low_value"))) {
				this.isSevere = true;
				break;
			}
		}
	}

	/**
	 * Calculate, if the hypo is probably the result of a previously exaggerated
	 * insuline bolus, resulting in a sharp descend of blood glucose in the time
	 * before the hypo.
	 */
	private void setOvercorrectionResult() {
		// Iterate backwards over readings and save the rate of descend. Evaluate the
		// peak point of bg before the hypo
		double lastValue = readings.get(startIndex).getCalculated_value();
		double currentValue = 0;
		double peakBgValue = 0;
		int descendCounter = 0;
		double descendSum = 0;
		for (int i = startIndex - 1; i <= 0; i--) {
			currentValue = readings.get(i).getCalculated_value();
			if (currentValue > peakBgValue) {
				peakBgValue = currentValue;
			}
			double difference = currentValue - lastValue;
			System.out.println(difference);
			if (difference > 0) {
				descendCounter++;
				descendSum += currentValue;
			}
		}
		double mediumDescendRate = descendSum / descendCounter;
		// Not working as intended yet. Reason: readings doesn't fit start and end index (fix in ExtremeValueCalculator)
		System.out.println("Peak ist: " + peakBgValue);
		System.out.println("Mittlere Sinkrate ist: " + mediumDescendRate);
	}
}

package dataModel;

import java.util.HashMap;

import Configuration.Constants;
import Configuration.Constants.IN_RANGE_VALUES;
import Configuration.propertiesHandler;
import dataModel.dataObjects.BgReadings;
import javafx.collections.ObservableList;

/**
 * This class calculates the data for the pie chart displaying the amount of
 * blood glucose values in "good" range, was well as below and above. A map of
 * these categories and the respective values is generated.
 * 
 * @author Benedikt Hotz
 *
 */
public class InRangeCalculator {

	private ObservableList<BgReadings> readings;

	public InRangeCalculator(ObservableList<BgReadings> readings) {
		super();
		this.readings = readings;
	}

	public HashMap<Constants.IN_RANGE_VALUES, Integer> calculateReadingsInRange() {

		// Initializations
		int readingBelow = 0;
		int readingIn = 0;
		int readingAbove = 0;

		// Iterate all blood glucose values and assign them to one of the fixed
		// categories
		for (BgReadings reading : readings) {
			if (reading.getCalculated_value() >= Integer
					.parseInt(propertiesHandler.getInstance().getPropteryByKey("Threshold_high"))) {
				readingAbove++;
			} else if (reading.getCalculated_value() <= Integer.parseInt(propertiesHandler.getInstance().getPropteryByKey("Threshold_low"))) {
				readingBelow++;
			} else {
				readingIn++;
			}
		}

		// Calculate percentages and return
		HashMap<Constants.IN_RANGE_VALUES, Integer> rv = new HashMap<Constants.IN_RANGE_VALUES, Integer>();
		rv.put(IN_RANGE_VALUES.BELOW, readingBelow * 100 / readings.size());
		rv.put(IN_RANGE_VALUES.IN, readingIn * 100 / readings.size());
		rv.put(IN_RANGE_VALUES.ABOVE, readingAbove * 100 / readings.size());
		return rv;
	}
}

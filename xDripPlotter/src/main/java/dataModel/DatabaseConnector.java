package dataModel;

import java.security.InvalidParameterException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import Configuration.Constants;
import dataModel.dataObjects.BgReadings;
import dataService.ComponentDao;
import dataService.config.JdbcDataTypes;
import dataService.syntaxComposer.DqlSyntaxComposer;
import dataService.syntaxComposer.SqlColumn;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Point of access to the database; uses DataService dependency for database
 * calls. Main purpose is to get readings from the DB - in order to do that, the
 * SQL syntax for the calls has to be set up accoring to DataService's
 * requirements.
 * 
 * @author Benedikt Hotz
 *
 */
public class DatabaseConnector {

	public DatabaseConnector(String dbPath) {
		ComponentDao.initSqlite("jdbc", dbPath);
	}

	/**
	 * This method constructs the required tokens for an SQL select call using
	 * DataService. Start and end date for the select have to be provided.
	 * 
	 * @param timeAfter  - LocalDateTime
	 * @param timeBefore - LocalDateTime
	 * @return {@link ObservableList} of {@link BgReadings} matching the given time
	 *         parameters
	 */
	public ObservableList<BgReadings> getReadings(LocalDateTime timeAfter, LocalDateTime timeBefore) {
		// Build syntax
		long begin = DateTimeUtility.toUnixTimestamp(timeAfter) * 1000;
		long end = DateTimeUtility.toUnixTimestamp(timeBefore) * 1000;
		DqlSyntaxComposer syntax = new DqlSyntaxComposer();
		syntax.setTable("BgReadings");
		SqlColumn col_reading_value = new SqlColumn("calculated_value");
		syntax.addCol(col_reading_value);
		SqlColumn col_reading_time = new SqlColumn("timestamp");
		syntax.addCol(col_reading_time);
		syntax.setFilter("timestamp > " + begin + " AND timestamp < " + end);

		// Data type for prep. statement parameter substitution
		JdbcDataTypes[] types = { JdbcDataTypes.Long, JdbcDataTypes.Long };

		// Get it! And convert it...
		ArrayList<Object> result = ComponentDao.getInstance().readValue(new BgReadings(), syntax, types);
		ObservableList<BgReadings> rv = FXCollections.observableArrayList();
		for (Object o : result) {
			rv.add((BgReadings) o);
		}
		return rv;
	}

	/**
	 * Retrieve the earliest or latest dataset in the database. Provide
	 * Constants.DATABASE_MAX_MIN to select the intended value.
	 * 
	 * @return LocalDate - the erliest or latest date in DB
	 */
	public LocalDate getMaxMinDataset(Constants.DATABASE_MAX_MIN maxMin) {
		DqlSyntaxComposer syntax = new DqlSyntaxComposer();
		syntax.setTable("BgReadings");
		if (maxMin.equals(Constants.DATABASE_MAX_MIN.LATEST)) {
			syntax.setMaxValue("timestamp");
		} else if (maxMin.equals(Constants.DATABASE_MAX_MIN.EARLIEST)) {
			syntax.setMinValue("timestamp");
		} else {
			throw new InvalidParameterException(
					"Parameter must be either Constants.DATABASE_MAX_MIN.EARLIEST or Constants.DATABASE_MAX_MIN.LATEST!");
		}
		ArrayList<Object> result = ComponentDao.getInstance().readValue(new BgReadings(), syntax);
		BgReadings readingsObj = (BgReadings) result.get(0);
		return DateTimeUtility.toLocalDate(readingsObj.getTimestamp());
	}

}

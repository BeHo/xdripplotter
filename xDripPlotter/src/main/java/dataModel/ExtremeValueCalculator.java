package dataModel;

import java.util.ArrayList;

import Configuration.Constants;
import Configuration.propertiesHandler;
import dataModel.dataObjects.BgReadings;
import dataModel.dataObjects.Hypoglycemia;
import javafx.collections.ObservableList;

/**
 * Given a list of blood glucose readings, this class is responsible for
 * scanning for hypoglycemias and providing a list of the respective outcome. It
 * relies on several thresholds, which are for the time being stored in
 * {@link Constants} interface - later, the user can set these threshold
 * manually (currently under construction)
 * 
 * @author Benedikt Hotz
 *
 */
public class ExtremeValueCalculator {

	private ObservableList<BgReadings> readings;
	private ArrayList<Hypoglycemia> hypos;

	public ExtremeValueCalculator(ObservableList<BgReadings> readings) {
		this.readings = readings;
		this.hypos = new ArrayList<Hypoglycemia>();
	}

	public ArrayList<Hypoglycemia> getHypos() {
		return hypos;
	}

	/**
	 * Scans the current readings field for hypoglycemia patterns. The method
	 * estimates a pattern based upon the following settings (currently all found in
	 * Constants interface):<br/>
	 * - the "low" threshold,<br/>
	 * - the amount of readings in a row that have to be below this threshold.<br/>
	 * More conditions may be added later.
	 */
	public void detectHypos() {
		int hypoCounter = 0;
		int startIndex = 0;
		int endIndex = 0;

		ArrayList<BgReadings> hypoValues = new ArrayList<BgReadings>();
		for (BgReadings reading : readings) {
			if (reading.getCalculated_value() < Integer.parseInt(propertiesHandler.getInstance().getPropteryByKey("Threshold_low"))) {
				if (startIndex == 0) { // If it's a new hypo...
					startIndex = readings.indexOf(reading) - 1; // Save index of last value before hypo
				}
				hypoCounter++;
				hypoValues.add(reading);
			} else {
				if (hypoCounter >= 3) {
					endIndex = readings.indexOf(reading); // Index of first value after hypo
					hypoValues = appendValuesAfterAndBeforeHypo(hypoValues, startIndex, endIndex);
					Hypoglycemia currentHypo = new Hypoglycemia(hypoValues, startIndex, endIndex);
					hypos.add(currentHypo);
				}
				hypoCounter = 0;
				startIndex = 0;
				hypoValues = new ArrayList<BgReadings>();
			}
		}
	}

	/**
	 * Helper method, but quite important: in order to be able to analyze the
	 * reasons for hypoglycemias later, we need some "history" or context.
	 * Therefore, we append the readings about 1,5 hours before and after a found
	 * hypo to the list of readings for this particular hypo, so we can see what may
	 * have caused it and what happened afterwards. Method needs the readings of the
	 * hypo itself, as well as the index of the first and last reading of the hypo
	 * in the overall readings list. Then, the mentioned additional values can be
	 * extracted from the overall list and appended to the hypo-readings-list.
	 * 
	 * @param hypoValues - ArrayList of BgReadings
	 * @param start      - int (index in overall readings list)
	 * @param end        - int (index in overall readings list)
	 * @return ArrayList of BgReadings (extended version of given hypoValues)
	 */
	private ArrayList<BgReadings> appendValuesAfterAndBeforeHypo(ArrayList<BgReadings> hypoValues, int start, int end) {
		for (int i = start; i > start - 18; i--) {
			hypoValues.add(0, readings.get(i));
		}
		for (int j = end; j <= end + 18; j++) {
			hypoValues.add(hypoValues.size(), readings.get(j));
		}
		return hypoValues;
	}
}

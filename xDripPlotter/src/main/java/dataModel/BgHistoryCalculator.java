package dataModel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import dataModel.dataObjects.BgReadings;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;

/**
 * This class prepares data for display in JavaFX line charts. These charts need
 * one or several series of data, each representing one line in the chart. In
 * this case, the blood glucose readings of one day are mapped to one chart
 * line.
 * 
 * @author Benedikt Hotz
 *
 */
public class BgHistoryCalculator {

	private ObservableList<BgReadings> readings;

	public BgHistoryCalculator(ObservableList<BgReadings> readings) {
		this.readings = readings;
	}

	/**
	 * Iterates the list of readings passed to the constructor and splits them
	 * day-wise, where the x-axis is the timestamp of the reading and the y-axis is
	 * the blood glucose value. JavaFX {@link Series} objects can be named (used for
	 * the explanation below the chart), we use the respective date here.<br/> Called by
	 * the refreshView() method of BgHistoryController once new data come in.
	 * 
	 * @return {@link List} of {@link Series} containing Number objects.
	 */
	public ArrayList<Series<Number, Number>> calculateSeries() {
		// Initializations
		ArrayList<Series<Number, Number>> rv = new ArrayList<Series<Number, Number>>();
		LocalDateTime savedDate = null;
		Series<Number, Number> currentSeries = new Series<Number, Number>();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E, dd. MMM yyyy");

		for (int i = 0; i < readings.size(); i++) {
			if (i == 0) {
				savedDate = DateTimeUtility.toLocalDateTime(readings.get(i).getTimestamp());
				currentSeries.setName(DateTimeUtility.toLocalDate(readings.get(i).getTimestamp()).format(formatter));
			}
			LocalDateTime currentDate = DateTimeUtility.toLocalDateTime(readings.get(i).getTimestamp());

			// Check if we have a new date - start a new series in that case
			if (LocalDate.from(currentDate).isAfter(LocalDate.from(savedDate))) {
				rv.add(currentSeries);
				currentSeries = new Series<Number, Number>();
				currentSeries.setName(DateTimeUtility.toLocalDate(readings.get(i).getTimestamp()).format(formatter));
				savedDate = currentDate;
			}
			long minuteOfDay = ChronoUnit.MINUTES.between(currentDate.toLocalDate().atStartOfDay(),
					DateTimeUtility.toLocalDateTime(readings.get(i).getTimestamp()));
			long reading = Math.round(readings.get(i).getCalculated_value());

			// Generate Data item, format it (by applying custom Node, Tooltip)
			Data<Number, Number> data = new Data<Number, Number>(minuteOfDay, reading);
			data.setNode(new guiController.extendedFxClasses.Tooltip(data));
			currentSeries.getData().add(data);

			// Handle last dataset, wouldn't be added to array otherwise.
			if (i == readings.size() - 1) {
				rv.add(currentSeries);
			}
		}

		return rv;
	}
}

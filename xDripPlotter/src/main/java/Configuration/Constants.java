package Configuration;

public interface Constants {
	String MAINGUI_FXML_LOCATION = "/fxml/xPlot_mainGui.fxml";
	String USERPREF_FXML_LOCATION = "/fxml/xPlot_userPref.fxml";
	String TIMEZONE = "Europe/Berlin";
	String DATETIMEFORMAT = "dd.MM.yyyy, HH:mm";
	String CONFIG_FILE_LOC = "UserPreferences.txt";
	int DEFAULT_TIMESPAN = 14;

	enum DATABASE_MAX_MIN {
		EARLIEST, LATEST
	};

	String PROPERTY_CHANGE_TIMESPAN = "timespan";
	String PROPERTY_CHANGE_FROM_DATE = "from";
	String PROPERTY_CHANGE_TO_DATE = "to";

	enum IN_RANGE_VALUES {
		BELOW, IN, ABOVE
	}
}

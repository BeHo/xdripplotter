package Configuration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeMap;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * A (Singleton) class to write and read user preferences in a properties file.
 * 
 * @author Benedikt Hotz
 *
 */
public class propertiesHandler {

	private static propertiesHandler instance = null;
	private HashMap<String, String> properties;

	private propertiesHandler() {
		super();
		properties = new HashMap<String, String>();
		setPropertyList();
	}

	public static propertiesHandler getInstance() {
		if (instance == null) {
			instance = new propertiesHandler();

		}
		return instance;
	}

	public HashMap<String, String> getPropertyList() {
		return properties;
	}

	/**
	 * Writes the given key/value pair to the properties file instantly. Calling
	 * this repeatedly might lead to a slight loss of performance because the method
	 * does not provide any buffering. Use writePropertyList() instead for multiple
	 * inserts. A general header text can be written or omitted (by passing null as
	 * third argument/header).
	 * 
	 * @param property String The key
	 * @param value    String The value
	 * @param header   String Header text for the whole file. May be null.
	 * @throws IOException
	 */
	public void writeProperty(String property, String value, String header) throws IOException {
		TreeMap<String, String> propertyValuePair = new TreeMap<String, String>();
		propertyValuePair.put(property, value);
		doWriteProperty(propertyValuePair, header);
		// FIXME Rewrite with another argument (boolean) - if true, make the helper
		// method search for existing properties of that name and update it instead of
		// rewriting the whole file with just that single property. Before that, check
		// if Properties class has some built-in mechanism for that.
	}

	/**
	 * Helper method bundling the actual writing to the properties file. Uses
	 * TreeMap, so 0-n properties can be written in a single call.
	 * 
	 * @param propertyValuePairs
	 * @param header
	 * @return Returncode as int - 1 is successful, 0 is fail
	 */
	private int doWriteProperty(TreeMap<String, String> propertyValuePairs, String header) {
		FileWriter writer = null;
		int rc = 0;
		try {
			// Prepare the properties file fpr reading first
			Properties props = new Properties();
			File propfile = new File(Constants.CONFIG_FILE_LOC);
			propfile.createNewFile();
			FileReader reader = new FileReader(propfile);
			props.load(reader);

			writer = new FileWriter(propfile);
			for (Map.Entry<String, String> pair : propertyValuePairs.entrySet()) {
				props.setProperty(pair.getKey(), pair.getValue());
				properties.put(pair.getKey(), pair.getValue());
			}
			props.store(writer, header);
		} catch (FileNotFoundException e) {
			showFileNotFoundError();
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				writer.close();
				rc = 1;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rc;
	}

	/**
	 * Writes the given set of key/value pairs to the properties file. An optional
	 * header text can be provided (pass null to skip). <b>Attention:</b> this will
	 * override any existing content and rebuild the properties file entirely!
	 * 
	 * @param list   - {@link HashMap} of key/value
	 * @param header - String
	 * @return Returncode as int - 1 if successful, 0 if failed
	 */
	public int writePropertyList(TreeMap<String, String> list, String header) {
		return doWriteProperty(list, header);
	}

	/**
	 * Get the value for the given property as String. The method is based on the
	 * respective class field, meaning that it won't perform any I/O action on the
	 * disk. If the "properties" field is still empty or the given property doesn't
	 * exist for some reason (like typos), an error message is displayed, followed
	 * by a nullpointer exception.
	 * 
	 * @param key - String - The property's key.
	 * @return value of the property as <b>String</b>
	 */
	public String getPropteryByKey(String key) {
		String rv = properties.get(key);

		// rv is null if the given property could not be found for some reason.
		// Display a warning message in that case and throw a Nullpointer Ex.
		if (rv == null) {
			showEntryNotFoundWarning(key);
			throw new NullPointerException("Unable to find value for given property" + key);
		}
		return rv;
	}

	/**
	 * Helper method; called if the properties field of this class is still empty.
	 */
	private void setPropertyList() {
		
		// Make sure the file exists to avoid FileNotFoundExceptions in situations where
		// the file just cannot exist (like first program start)
		try {
			File propFile = new File(Constants.CONFIG_FILE_LOC);
			propFile.createNewFile();
			FileReader reader = new FileReader(propFile);
			Properties props = new Properties();
			props.load(reader);
			for (Entry<Object, Object> entry : props.entrySet()) {
				properties.put(entry.getKey().toString(), entry.getValue().toString());
			}
		} catch (FileNotFoundException e) {
			// This is not likely. Could be true if someone messes with the file system during execution
			showFileNotFoundError();
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showFileNotFoundError() {
		Alert noPropFileError = new Alert(AlertType.ERROR);
		noPropFileError.setHeaderText(null);
		noPropFileError.setTitle("Datei nicht gefunden");
		noPropFileError.setContentText("Die Datei " + Constants.CONFIG_FILE_LOC
				+ " wurde nicht gefunden. Bitte stellen Sie sicher, dass die Datei nicht gelöscht oder umbenannt wurde.");
		noPropFileError.show();
	}

	private void showEntryNotFoundWarning(String key) {
		Alert noSuchPropWarning = new Alert(AlertType.WARNING);
		noSuchPropWarning.setHeaderText(null);
		noSuchPropWarning.setTitle("Einstellung nicht gefunden");
		noSuchPropWarning.setContentText("Die Nutzereinstellung \"" + key
				+ "\" wurde nicht gefunden. Entweder wurde sie nie gesetzt, oder die Einstellungsdatei "
				+ Constants.CONFIG_FILE_LOC
				+ " wurde in unzuläsiger Weise verändert. Stellen Sie sicher, dass die gewünschte Eigenschaft in der Datei belegt ist (Schema: <Eigenschaft>=<Wert>) und versuchen sie es erneut!");
		noSuchPropWarning.showAndWait();
	}
}

package guiEventHandlers;

import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.events.Event;
import com.itextpdf.kernel.events.IEventHandler;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;

public class NewPdfPageHandler implements IEventHandler {

	@Override
	public void handleEvent(Event event) {
		PdfDocumentEvent pageTurner = (PdfDocumentEvent) event;
		PdfCanvas frame = new PdfCanvas(pageTurner.getPage());
		frame.roundRectangle(40, 40, PageSize.A4.getWidth() - 80, PageSize.A4.getHeight() - 80, 40);
		frame.setStrokeColor(new DeviceRgb(75, 129, 210));
		frame.setLineWidth(5);
		frame.stroke();
	}

}

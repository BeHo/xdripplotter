package guiLauncher;

import Configuration.Constants;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Launch the JavaFX application. This is done in a separate class because there
 * is some issue with Maven and Main classes extending {@link Application}
 * directly.
 * 
 * @author Benedikt Hotz
 *
 */
public class ApplicationLauncher extends Application {

	/**
	 * Start the application: load xPlot_mainGui.fxml, set title, set to fullscreen.
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(getClass().getResource(Constants.MAINGUI_FXML_LOCATION));
		Parent root = loader.load();
		primaryStage.setTitle("xPlot - xDrip+ Auswertungen am PC");
		primaryStage.setMaximized(true);
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}

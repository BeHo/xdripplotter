# xDripPlotter

A (currently) very basic plotting tool that lets you map exported xDrip+ databases (SQLite) to nice charts for further analysis. It is aimed at users (==> me :-D) who don't want to rely on cloud based visualization tools for medic data.